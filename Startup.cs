using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Marten;
using Microsoft.AspNetCore.Hosting;

namespace Sample
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app)
        {
            app.UseMvc();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            // Marten document store
            services.AddScoped<IDocumentStore>(provider => 
                DocumentStore.For("Server=127.0.0.1;Port=5432;Database=achievedb;User Id=admin;Password=admin;"));
        }
    }
}